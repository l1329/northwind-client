package com.ulasalle.lp3.evaluationfinal.model;

import lombok.Getter;
import lombok.Setter;
import lombok.Builder;

@Getter
@Setter
@Builder

public class Category {
    private String description;
    private Integer id;
    private String name;
}
