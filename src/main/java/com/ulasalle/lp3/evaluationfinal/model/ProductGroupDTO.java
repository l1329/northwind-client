package com.ulasalle.lp3.evaluationfinal.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@Builder
public class ProductGroupDTO {
    private Integer totalProducts;
    private List<Products> products;
}
