package com.ulasalle.lp3.evaluationfinal.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Setter
@Getter
public class Product {

    private Category category;
    private Integer categoryId;
    private Supplier supplier;
    private Integer supplierId;
    private Integer id;
    private String name;
    private Double price;
    private String quantityPerUnit;
    private Integer stock;

}
