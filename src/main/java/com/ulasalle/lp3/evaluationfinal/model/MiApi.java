package com.ulasalle.lp3.evaluationfinal.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MiApi {
    private String url;
    private Class typeClass;
    private Map<String, String> mapVariables;
    private MultiValueMap<String, String> mapParameter;
    private String exception;
    private HttpHeaders httpHeaders;
    private String token;


    public URI getUri() {
        if (mapVariables == null)
            mapVariables = new HashMap<>();

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(this.url).queryParams(mapParameter);
        return builder.buildAndExpand(mapVariables).toUri();
    }

    public HttpHeaders getHttpHeaders() {
        httpHeaders = httpHeaders == null ? new HttpHeaders() : httpHeaders;
        if (token != null && !token.isEmpty())
            httpHeaders.setBearerAuth(token);

        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        return httpHeaders;
    }


}
