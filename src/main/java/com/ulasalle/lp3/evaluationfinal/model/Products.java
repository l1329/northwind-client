package com.ulasalle.lp3.evaluationfinal.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class Products {
    private Integer id;
    private String name;
    private Double price;
    private String quantityPerUnit;
    private Integer stock;
}
