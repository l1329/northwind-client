package com.ulasalle.lp3.evaluationfinal.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@AllArgsConstructor
@ToString
public class User {

    private Integer id;

    @NotBlank(message = "Name is mandatory")
    private String name;

    @NotBlank(message = "Email is mandatory")
    private String email;

    public static User USER_1 = new User(1,"Usuario1","Usuario@gmail.com");
    public static User USER_2 = new User(2,"Usuario2","Usuario2@gmail.com");
    public static User USER_3 = new User(3,"Usuario3","Usuario3@gmail.com");


}
