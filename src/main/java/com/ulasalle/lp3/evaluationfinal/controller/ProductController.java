package com.ulasalle.lp3.evaluationfinal.controller;

import com.ulasalle.lp3.evaluationfinal.model.Product;
import com.ulasalle.lp3.evaluationfinal.model.ProductGroupDTO;
import com.ulasalle.lp3.evaluationfinal.model.User;
import com.ulasalle.lp3.evaluationfinal.service.CategoryService;
import com.ulasalle.lp3.evaluationfinal.service.ProductService;
import com.ulasalle.lp3.evaluationfinal.service.SupplerService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.swing.*;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequiredArgsConstructor
@RequestMapping("product")
public class ProductController {

    private final ProductService productService;
    private final CategoryService categoryService;
    private final SupplerService supplerService;

    @GetMapping("/products")
    public String showProductsList(@RequestParam(required = false, defaultValue = "0") int page,
                                   @RequestParam(required = false, defaultValue = "10") int rows,
                                   Model model) {
        ProductGroupDTO productGroupDTO =  productService.findAll(page);
        model.addAttribute("products",productGroupDTO.getProducts());
        model.addAttribute("pages",buildPages(productGroupDTO.getTotalProducts()));
        return "products";
    }

    @GetMapping("/signup")
    public String showSignUpForm(Model model) {
        model.addAttribute("product", Product.builder().build() );
        model.addAttribute("categories",categoryService.findAll() );
        model.addAttribute("suppliers",supplerService.findAll() );
        return "add-product";
    }

    @PostMapping("/addProduct")
    public String add(Product product, Model model) {
        productService.create(product);
        return "redirect:/product/products";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") Integer id, Model model) {
        productService.delete(id);
        return "redirect:/product/products";
    }

    @GetMapping("/edit/{id}")
    public String showUpdateForm(@PathVariable("id") Integer id, Model model) {
        Product product = productService.findById(id);
        model.addAttribute("product", product);
        model.addAttribute("categories",categoryService.findAll() );
        model.addAttribute("suppliers",supplerService.findAll() );
        return "update-product";
    }

    @PostMapping("/update/{id}")
    public String updatePriceStock( @PathVariable("id") Integer id,Product product, Model model) {
        productService.update(id, product);
        return "redirect:/product/products";
    }




    private List<Integer> buildPages(Integer size){
        List<Integer> list = new ArrayList<>();
        Integer i = 0;
        while (true){
            list.add(i);
            i++;
            if(list.size()*15>size)
                break;
        }
        return list;
    }

}

