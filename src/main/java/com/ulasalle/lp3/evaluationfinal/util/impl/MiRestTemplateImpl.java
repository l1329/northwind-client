package com.ulasalle.lp3.evaluationfinal.util.impl;


import com.ulasalle.lp3.evaluationfinal.model.MiApi;
import com.ulasalle.lp3.evaluationfinal.util.MiRestTemplate;

import lombok.RequiredArgsConstructor;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@RequiredArgsConstructor
public class MiRestTemplateImpl implements MiRestTemplate {

    private final RestTemplate restTemplate;


    @Override
    public <T> T get(MiApi api) {
        HttpHeaders headers =  api.getHttpHeaders();
        HttpEntity<String> entity = new HttpEntity<>(headers);
        ResponseEntity<T> responseEntity =  restTemplate.exchange(api.getUri(), HttpMethod.GET, entity, api.getTypeClass());
        return responseEntity.getBody();
    }

    @Override
    public <T,V> T post(MiApi api, V object) {
        HttpHeaders headers = api.getHttpHeaders();
        HttpEntity<V> entity = new HttpEntity<>(object, headers);
        ResponseEntity<T> responseEntity =  restTemplate.exchange(api.getUri(), HttpMethod.POST, entity, api.getTypeClass());
        return responseEntity.getBody();
    }

    @Override
    public <T,V> T put(MiApi api, V object) {
        HttpHeaders headers = api.getHttpHeaders();
        HttpEntity<V> entity = new HttpEntity<>(object, headers);
        ResponseEntity<T> responseEntity =  restTemplate.exchange(api.getUri(), HttpMethod.PUT, entity, api.getTypeClass());
        return responseEntity.getBody();
    }


    @Override
    public <T> T delete(MiApi api) {
        HttpEntity<String> entity = new HttpEntity<>(api.getHttpHeaders());
        ResponseEntity<T> responseEntity =  restTemplate.exchange(api.getUri(), HttpMethod.DELETE, entity, api.getTypeClass());
        return responseEntity.getBody();
    }




}
