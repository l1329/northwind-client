package com.ulasalle.lp3.evaluationfinal.util;


import com.ulasalle.lp3.evaluationfinal.model.MiApi;
import org.springframework.http.ResponseEntity;

public interface MiRestTemplate {

    <T> T get(MiApi api);

    <T, V> T post(MiApi api, V object);

    <T, V> T put(MiApi api, V object);

    <T> T delete(MiApi api);
}
