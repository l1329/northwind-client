package com.ulasalle.lp3.evaluationfinal.service;

import com.ulasalle.lp3.evaluationfinal.model.User;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private List<User> users;

    private void init() {
        if (users != null)
            return;
        users = new ArrayList<>();
        users.add(User.USER_1);
        users.add(User.USER_2);
        users.add(User.USER_3);
    }

    @Override
    public List<User> findAll() {
        init();
        return users;
    }

    @Override
    public User findById(Integer id) {
        init();
        return users.stream().filter(user -> user.getId() == id).findFirst().orElse(null);
    }

    @Override
    public void addUser(User user) {
        init();
        user.setId(users.size()-1);
        users.add(user);
    }

    @Override
    public void update(Integer id, User user) {
        init();
        users.forEach(user1 -> {
            if (user1.getId() == id) {
                user1.setName(user.getName());
                user1.setEmail(user.getEmail());
            }
        });
    }

    @Override
    public void delete(Integer id) {
        init();
        users.remove(findById(id));
    }
}
