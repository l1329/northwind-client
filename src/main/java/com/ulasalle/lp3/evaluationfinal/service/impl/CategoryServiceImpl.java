package com.ulasalle.lp3.evaluationfinal.service.impl;

import com.ulasalle.lp3.evaluationfinal.client.CategoryClient;
import com.ulasalle.lp3.evaluationfinal.model.Category;
import com.ulasalle.lp3.evaluationfinal.service.CategoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
@RequiredArgsConstructor
public class CategoryServiceImpl implements CategoryService {

    private final CategoryClient client;

    @Override
    public List<Category> findAll() {
        return client.findAll();
    }
}
