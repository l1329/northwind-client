package com.ulasalle.lp3.evaluationfinal.service;

import com.ulasalle.lp3.evaluationfinal.model.Supplier;

import java.util.List;

public interface SupplerService {

    List<Supplier> findAll();
}
