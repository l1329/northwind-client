package com.ulasalle.lp3.evaluationfinal.service;

import com.ulasalle.lp3.evaluationfinal.model.User;

import java.util.List;

public interface UserService {
    List<User> findAll();

    User findById(Integer id);

    void addUser(User id);

    void update(Integer id, User user);

    void delete(Integer id);


}
