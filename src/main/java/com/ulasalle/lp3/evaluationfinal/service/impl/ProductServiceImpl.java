package com.ulasalle.lp3.evaluationfinal.service.impl;

import com.ulasalle.lp3.evaluationfinal.client.ProductsClient;
import com.ulasalle.lp3.evaluationfinal.model.*;
import com.ulasalle.lp3.evaluationfinal.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

    private final ProductsClient client;

    @Override
    public ProductGroupDTO findAll(@Nullable Integer page) {
        return client.getFindAll(page == null ? 0 : page);
    }

    @Override
    public Product create(Product product) {
        product.setCategory(Category.builder().id(product.getCategoryId()).build());
        product.setSupplier(Supplier.builder().id(product.getSupplierId()).build());
        return client.postCreate(product);
    }

    @Override
    public void delete(Integer id) {
        client.delete(id);
    }

    @Override
    public Product findById(Integer id) {
        Product product = client.getFindById(id);
        product.setCategoryId(product.getCategory().getId());
        product.setSupplierId(product.getSupplier().getId());
        return product;
    }

    @Override
    public void update(Integer id, Product product) {
        client.putPrice(id, product);
        client.putStock(id, product);
    }
}
