package com.ulasalle.lp3.evaluationfinal.service;

import com.ulasalle.lp3.evaluationfinal.model.Category;

import java.util.List;

public interface CategoryService {
    List<Category> findAll();
}
