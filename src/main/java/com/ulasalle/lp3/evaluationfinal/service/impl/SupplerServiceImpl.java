package com.ulasalle.lp3.evaluationfinal.service.impl;

import com.ulasalle.lp3.evaluationfinal.client.SupplerClient;
import com.ulasalle.lp3.evaluationfinal.model.Supplier;
import com.ulasalle.lp3.evaluationfinal.service.SupplerService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class SupplerServiceImpl implements SupplerService {

    private final SupplerClient client;

    @Override
    public List<Supplier> findAll() {
        return client.findAll();
    }
}
