package com.ulasalle.lp3.evaluationfinal.service;

import com.ulasalle.lp3.evaluationfinal.model.Product;
import com.ulasalle.lp3.evaluationfinal.model.ProductGroupDTO;
import com.ulasalle.lp3.evaluationfinal.model.Products;
import com.ulasalle.lp3.evaluationfinal.model.User;

import java.util.List;

public interface ProductService {
    ProductGroupDTO findAll(Integer page);
    Product create(Product product);
    void delete(Integer id);
    Product findById(Integer id);
    void update(Integer id, Product product);
}
