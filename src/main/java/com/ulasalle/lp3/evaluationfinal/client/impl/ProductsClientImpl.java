package com.ulasalle.lp3.evaluationfinal.client.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ulasalle.lp3.evaluationfinal.client.ProductsClient;
import com.ulasalle.lp3.evaluationfinal.model.MiApi;
import com.ulasalle.lp3.evaluationfinal.model.Product;
import com.ulasalle.lp3.evaluationfinal.model.ProductGroupDTO;
import com.ulasalle.lp3.evaluationfinal.model.Products;
import com.ulasalle.lp3.evaluationfinal.util.MiRestTemplate;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.HashMap;
import java.util.List;

@Component
@RequiredArgsConstructor
@Log4j2
public class ProductsClientImpl implements ProductsClient {

    private final MiRestTemplate restTemplate;
    private final ObjectMapper mapper;

    @Override
    public ProductGroupDTO getFindAll(Integer page) {
        try {
            MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
            map.add("page", page.toString());
            map.add("rows", "15");


            MiApi api = MiApi.builder()
                    .url("https://northwind-server.herokuapp.com/api/northwind/v1/product")
                    .typeClass(JsonNode.class)
                    .mapParameter(map)
                    .build();
            JsonNode json = restTemplate.get(api);
            ProductGroupDTO products = mapper.readValue(json.get("data").toPrettyString(), ProductGroupDTO.class);
            return products;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    @Override
    public Product postCreate(Product product) {
        try {
            MiApi api = MiApi.builder()
                    .url("https://northwind-server.herokuapp.com/api/northwind/v1/product")
                    .typeClass(JsonNode.class)
                    .build();
            JsonNode json = restTemplate.post(api,product);
            Product productRQ = mapper.readValue(json.get("data").toPrettyString(), Product.class);
            return productRQ;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void delete(Integer id) {
        HashMap map = new HashMap();
        map.put("id",id);

        MiApi api = MiApi.builder()
                .url("https://northwind-server.herokuapp.com/api/northwind/v1/product/{id}")
                .typeClass(JsonNode.class)
                .mapVariables(map)
                .build();
        JsonNode json = restTemplate.delete(api);
        log.info(json.toPrettyString());
    }


    @Override
    public Product getFindById(Integer id) {
        try {
            HashMap map = new HashMap();
            map.put("id",id);

            MiApi api = MiApi.builder()
                    .url("https://northwind-server.herokuapp.com/api/northwind/v1/product/{id}")
                    .typeClass(JsonNode.class)
                    .mapVariables(map)
                    .build();
            JsonNode json = restTemplate.get(api);
            Product product = mapper.readValue(json.get("data").toPrettyString(), Product.class);
            return product;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void putPrice(Integer id, Product product) {
        try {
            HashMap map = new HashMap();
            map.put("id",id);

            MiApi api = MiApi.builder()
                    .url("https://northwind-server.herokuapp.com/api/northwind/v1/product/price/{id}")
                    .typeClass(JsonNode.class)
                    .mapVariables(map)
                    .build();
            JsonNode json = restTemplate.put(api,product);
            Product productRQ = mapper.readValue(json.get("data").toPrettyString(), Product.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void putStock(Integer id, Product product) {
        try {
            HashMap map = new HashMap();
            map.put("id",id);

            MiApi api = MiApi.builder()
                    .url("https://northwind-server.herokuapp.com/api/northwind/v1/product/stock/{id}")
                    .typeClass(JsonNode.class)
                    .mapVariables(map)
                    .build();
            JsonNode json = restTemplate.put(api,product);
            Product productRQ = mapper.readValue(json.get("data").toPrettyString(), Product.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
