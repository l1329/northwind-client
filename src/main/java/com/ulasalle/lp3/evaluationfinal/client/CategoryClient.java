package com.ulasalle.lp3.evaluationfinal.client;


import com.ulasalle.lp3.evaluationfinal.model.Category;

import java.util.List;

public interface CategoryClient {

    List<Category> findAll();

}
