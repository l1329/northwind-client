package com.ulasalle.lp3.evaluationfinal.client.impl;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ulasalle.lp3.evaluationfinal.client.SupplerClient;
import com.ulasalle.lp3.evaluationfinal.model.Category;
import com.ulasalle.lp3.evaluationfinal.model.MiApi;
import com.ulasalle.lp3.evaluationfinal.model.Supplier;
import com.ulasalle.lp3.evaluationfinal.util.MiRestTemplate;
import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
@RequiredArgsConstructor
@Log4j2
public class SupplerClientImpl implements SupplerClient {

    private final MiRestTemplate restTemplate;
    private final ObjectMapper mapper;

    @Override
    public List<Supplier> findAll() {
        try {
            MiApi api = MiApi.builder()
                    .url("https://northwind-server.herokuapp.com/api/northwind/v1/supplier")
                    .typeClass(JsonNode.class)
                    .build();
            JsonNode json = restTemplate.get(api);

            Supplier[] suppliers = mapper.readValue(json.get("data").toPrettyString(), Supplier[].class);
            return Arrays.asList(suppliers);
        } catch (Exception e) {
            log.error("error in {}",e.getMessage());
            return null;
        }
    }
}
