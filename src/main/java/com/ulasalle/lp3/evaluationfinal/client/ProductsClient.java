package com.ulasalle.lp3.evaluationfinal.client;

import com.ulasalle.lp3.evaluationfinal.model.Product;
import com.ulasalle.lp3.evaluationfinal.model.ProductGroupDTO;


public interface ProductsClient{
        ProductGroupDTO getFindAll(Integer page);
        Product postCreate(Product product);
        void delete(Integer id);
        Product getFindById(Integer id);
        void putPrice(Integer id, Product product);
        void putStock(Integer id, Product product);
}
