package com.ulasalle.lp3.evaluationfinal.client.impl;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ulasalle.lp3.evaluationfinal.client.CategoryClient;
import com.ulasalle.lp3.evaluationfinal.model.Category;
import com.ulasalle.lp3.evaluationfinal.model.MiApi;
import com.ulasalle.lp3.evaluationfinal.model.ProductGroupDTO;
import com.ulasalle.lp3.evaluationfinal.util.MiRestTemplate;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
@Component
@RequiredArgsConstructor
public class CategoryClientImpl implements CategoryClient {

    private final MiRestTemplate restTemplate;
    private final ObjectMapper mapper;

    @Override
    public List<Category> findAll() {
        try {
            MiApi api = MiApi.builder()
                    .url("https://northwind-server.herokuapp.com/api/northwind/v1/category")
                    .typeClass(JsonNode.class)
                    .build();
            JsonNode json = restTemplate.get(api);

            Category[] categories = mapper.readValue(json.get("data").toPrettyString(), Category[].class);
            return Arrays.asList(categories);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
