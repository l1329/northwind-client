package com.ulasalle.lp3.evaluationfinal.client;

import com.ulasalle.lp3.evaluationfinal.model.Supplier;

import java.util.List;

public interface SupplerClient {
    List<Supplier> findAll();
}
